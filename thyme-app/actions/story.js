import { AsyncStorage } from 'react-native'
import gql from 'graphql-tag'

import NavigationService from '../NavigationService'

export const create_story = (name, color) => {
  return async () => {

    const createMutation = gql`
      mutation{
        createStory(name: "${name}", color: "${color}"){
          name
        }
      }
    `

    await window.client.mutate({
      mutation: createMutation
    })

    NavigationService.navigate('Main')

  }
}
