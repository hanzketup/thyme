import { AsyncStorage } from 'react-native'
import gql from 'graphql-tag'

import NavigationService from '../NavigationService'

export const check_authentication = () => {
  return async () => {
    const token = await AsyncStorage.getItem('token');
    if(token === null){NavigationService.navigate('Login')}
  }
}

export const retrive_token = (username, password) => {
  return async () => {
    // remove the token before sending the new login request
    await AsyncStorage.setItem('token', '')
    const loginMutation = gql`
      mutation{
        signIn(login: "${username}", password: "${password}"){
          token
        }
      }
    `

    let response = await window.client.mutate({
      mutation: loginMutation
    })

    console.log(response.data.signIn.token)
    await AsyncStorage.setItem('token', response.data.signIn.token)
    NavigationService.navigate('Main')

  }
}
