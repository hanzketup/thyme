let toast_id = 0

export const add_toast = toast => {
  return dispatch => {
    let id = toast_id++
    setTimeout(() => dispatch(remove_toast(id)), 2000)
    dispatch({type: "ADD_TOAST", payload: {...toast, id: id}})
  }
}

export const remove_toast = id => {
  return {type: "REMOVE_TOAST", payload: id}
}
