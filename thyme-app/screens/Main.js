import React, {Component} from 'react'
import styled from 'styled-components/native'
import { ScrollView, Modal, Text } from 'react-native'

import Menu from '../components/Menu'
import MenuModal from '../components/MenuModal'
import ActiveStory from '../containers/ActiveStory'
import StoryList from '../containers/StoryList'

const MainView = styled.View`
  flex: 1;
`

const InnerView = styled.View`
  flex-grow: 1;
  padding: 0 10px 20px 10px;
`

class Main extends Component {

  render () {
    return (
      <MainView>

        {/*<MenuModal title="Story settings" />*/}

        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          <InnerView>
            <Menu />
            <ActiveStory />
            <StoryList />
          </InnerView>
        </ScrollView>
      </MainView>
    )
  }

}

export default Main
