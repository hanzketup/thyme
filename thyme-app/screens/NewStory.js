import React, {Component} from 'react'
import styled from 'styled-components/native'

import NavigationService from '../NavigationService'

import ScrollAndAvoidKeyboard from '../components/ScrollAndAvoidKeyboard'
import CloseButton from '../components/CloseButton'
import NewStoryForm from '../containers/NewStoryForm'


class NewStory extends Component {

  render () {
    return (
      <ScrollAndAvoidKeyboard>
        <CloseButton onPress={() => NavigationService.navigate("Main")} />
        <NewStoryForm />
      </ScrollAndAvoidKeyboard>
    )
  }

}

export default NewStory
