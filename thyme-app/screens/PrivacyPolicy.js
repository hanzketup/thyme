import React, {Component} from 'react'
import styled from 'styled-components/native'
import RNMarkdownFormatter from 'react-native-markdown-formatter'
import { ScrollView, Modal, Text } from 'react-native'

import privacy_policy from '../constants/privacy_policy'
import NavigationService from '../NavigationService'

import ScrollAndAvoidKeyboard from '../components/ScrollAndAvoidKeyboard'
import LargeButton from '../components/LargeButton'

const Wrap = styled.View`
  flex: 1;
  padding: 12% 5%;
`


class PrivacyPolicy extends Component {

  render () {
    return (
      <ScrollAndAvoidKeyboard>
        <Wrap>
          <RNMarkdownFormatter text={privacy_policy} />
          <LargeButton center
            title='Back to signup'
            icon='check'
            color='#786fa6'
            onPress={() => NavigationService.navigate('SignUp')}
          />
        </Wrap>
      </ScrollAndAvoidKeyboard>
    )
  }

}

export default PrivacyPolicy
