import React, {Component} from 'react'
import styled from 'styled-components/native'

import NavigationService from '../NavigationService'

import CloseButton from '../components/CloseButton'
import ScrollAndAvoidKeyboard from '../components/ScrollAndAvoidKeyboard'
import SignUpForm from '../containers/SignUpForm'

class Login extends Component {

  render () {
    return (
      <ScrollAndAvoidKeyboard>
        <CloseButton onPress={() => NavigationService.navigate("Login")} />
        <SignUpForm />
      </ScrollAndAvoidKeyboard>
    )
  }

}

export default Login
