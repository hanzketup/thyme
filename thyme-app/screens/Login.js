import React, {Component} from 'react'
import { KeyboardAvoidingView } from 'react-native'
import styled from 'styled-components/native'

import NavigationService from '../NavigationService'

import ScrollAndAvoidKeyboard from '../components/ScrollAndAvoidKeyboard'
import LoginForm from '../containers/LoginForm'
import LargeButton from '../components/LargeButton'

const Wrapper = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0;
  margin-top: 20%;
`

const Logo = styled.Image`
  height: 95px;
`

const Title = styled.Text`
  font-size: 46px;
  color: #6c757d;
  margin: 4px 0 8px 0;
  color: rgba(0, 0, 0, 0.6);
`

const OrDivider = styled.View`
  width: 60%;
  height: 2px;
  margin: 20px 0;
  background-color: #ecf0f1;
`

const SignUpText = styled.Text`
  text-align: center;
  color: #6c757d;
  font-size: 18px;
  padding: 1% 15% 0 15%;
`

class Login extends Component {

  render () {
    return (
      <ScrollAndAvoidKeyboard>
        <Wrapper>
          <Logo source={require('../assets/leaves.png')} resizeMode='contain' />

          <Title>Login</Title>
          <LoginForm />

          <OrDivider />

          <SignUpText>No account yet?</SignUpText>

          <LargeButton center
            title="Sign up"
            icon="user-plus"
            color="#74b9ff"
            onPress={() => NavigationService.navigate("SignUp")}
          />
        </Wrapper>
      </ScrollAndAvoidKeyboard>
    )
  }

}

export default Login
