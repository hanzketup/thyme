
let init = {
  toasts: []
}

export default (state = init, action) => {
  switch (action.type) {

    case 'ADD_TOAST':
      return {...state, toasts: [...state.toasts, action.payload]}

    case 'REMOVE_TOAST':
      return {...state, toasts: state.toasts.filter(x => x.id !== action.payload)}

    default:
      return state
  }
}
