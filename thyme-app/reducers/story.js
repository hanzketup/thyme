
let init = {
  modal_open: false
}

export default (state = init, action) => {
  switch (action.type) {

    case 'SET_STORY_MODAL_OPEN':
      return {...state, modal_open: action.payload}

    default:
      return state
  }
}
