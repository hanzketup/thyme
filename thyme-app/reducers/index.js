import { combineReducers } from 'redux'

import ui from './ui'
import story from './story'

const rootReducer = combineReducers({
  ui,
  story
})

export default rootReducer
