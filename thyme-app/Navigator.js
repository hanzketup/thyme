import React from 'react'
import { createStackNavigator, createSwitchNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation'

import Main from './screens/Main'
import Login from './screens/Login'
import SignUp from './screens/SignUp'
import NewStory from './screens/NewStory'
import PrivacyPolicy from './screens/PrivacyPolicy'

export default createAppContainer(
  createSwitchNavigator({
      Main: {
        screen: Main,
      },
      Login: {
        screen: Login
      },
      SignUp: {
        screen: SignUp
      },
      NewStory: {
        screen: NewStory
      },
      PrivacyPolicy: {
        screen: PrivacyPolicy
      }
    },
    {initialRouteName: 'Main'}
  )
)
