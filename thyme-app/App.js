import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import styled from 'styled-components/native'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fal } from '@fortawesome/pro-light-svg-icons'
import { AsyncStorage, View, SafeAreaView, StatusBar } from 'react-native'

import { ApolloClient, ApolloLink, HttpLink, InMemoryCache } from 'apollo-boost'
import { split } from 'apollo-link'
import { onError } from 'apollo-link-error'
import { setContext } from 'apollo-link-context'
import { ApolloProvider } from 'react-apollo'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'

import reducer from './reducers/index'
import * as uiActions from './actions/ui'
import Navigator from './Navigator'
import NavigationService from './NavigationService'
import Root from './containers/Root'

library.add(fal)
const store = createStore(reducer, applyMiddleware(thunk))

const authLink = setContext(async (_, { headers }) => {
  const token = await AsyncStorage.getItem('token')
  return {
    headers: {
      ...headers,
      'x-token': token ? token : ''
    }
  }
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(err => {
      if (err.extensions.code === 'UNAUTHENTICATED') {
        NavigationService.navigate('Login')
      }
      store.dispatch(uiActions.add_toast({color: "#c44569", message: `GraphQL error: ${err.message}`}))
      console.log(
        `[GraphQL error]: Message: ${err.message}, Location: ${err.locations}, Path: ${err.path}`
      )
    })
  }
  if (networkError) console.log(`[Network error]: ${networkError}`)
})

const httpLink = authLink.concat(new HttpLink({uri: 'http://192.168.1.181:4000/graphql'}))

const wsLink = new WebSocketLink({
  uri: `ws://192.168.1.181:4000/graphql`,
  options: {
    reconnect: true
  }
})

window.client = new ApolloClient({
  link: ApolloLink.from([errorLink, split(
      ({ query }) => {
        const definition = getMainDefinition(query)
        return (
          definition.kind === 'OperationDefinition' &&
          definition.operation === 'subscription'
        )
      },
      wsLink,
      httpLink
    )]
  ),
  cache: new InMemoryCache()
})

export default function App () {
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <Root>
          <SafeAreaView style={{flex: 1}}>
            <Navigator ref={navigatorRef => NavigationService.setTopLevelNavigator(navigatorRef)} />
          </SafeAreaView>
        </Root>
      </Provider>
    </ApolloProvider>
  )
}
