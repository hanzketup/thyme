import React from 'react'
import styled from 'styled-components/native'
import { lighten } from "polished"
import { StyleSheet, View, Text } from "react-native"
import { LinearGradient } from 'expo-linear-gradient'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

import { seconds_to_hours_minutes } from '../helpers/format_time'

const Wrapper = styled.TouchableHighlight`
  position: relative;
  width: 49%;
  height: 120px;
  background-color: ${x => x.color || "#95a5a6"};
  border-radius: 8px;
  elevation: 5;
  margin: 1% 0;
  opacity: ${x => x.active ? 0.3 : 0.9};
`

const Name = styled.Text`
  width: 60%;
  position: absolute;
  bottom: 2%;
  left: 5%;

  font-size: 20px;
  color: #fff;
  opacity: 0.8;
  text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
`

const Hours = styled.Text`
  position: absolute;
  top: 4%;
  right: 5%;

  font-size: 28px;
  font-weight: bold;
  color: #000;
  opacity: 0.12;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
`

const ContextMenu = styled.TouchableOpacity`
  position: absolute;
  top: 8%;
  left: 2%;
  padding: 2%;
`

const Active = styled.View`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height: 100%;
  width: 100%;
  justify-content: center;
  align-items: center;
`

const ActiveLabel = styled.Text`
  font-size: 26px;
  font-weight: bold;
  color: #333;
  padding: 2px 8px;
  border: 3px solid #333;
  border-radius: 4px;
  text-align: center;
  transform: rotateZ(-10deg);
`

export default props =>
  <Wrapper onPress={props.onPress} color={lighten(0.1, props.color)} active={props.active}>
    <View style={{flex: 1}}>
      {props.active && <Active><ActiveLabel>Active</ActiveLabel></Active>}
      <Name>{props.name}</Name>
      <Hours>{seconds_to_hours_minutes(props.seconds)}H</Hours>
      <ContextMenu onPress={props.contextOnPress}>
        <FontAwesomeIcon icon={["fal", "ellipsis-v"]} size={30} color="rgba(0, 0, 0, 0.3)" />
      </ContextMenu>
    </View>
  </Wrapper>
