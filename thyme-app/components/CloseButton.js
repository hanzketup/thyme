import React, { Component } from 'react'
import styled from 'styled-components/native'
import Constants from 'expo-constants'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

const Close = styled.TouchableOpacity`
  position: absolute;
  top: ${Constants.statusBarHeight}px;
  right: 0px;
  padding: 15px;
`

export default props =>
  <Close onPress={props.onPress} underlayColor="#ea8685">
    <FontAwesomeIcon icon={["fal", "times-circle"]} size={40} color="rgba(0, 0, 0, 0.4)" />
  </Close>
