import React from "react"
import styled from 'styled-components/native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

const Label = styled.Text`
  font-size: 18px;
  font-weight: 700;
  color: #596275;
  margin: 8px 0 5px 0;
  ${x => x.left && `margin-right: auto;`}
  ${x => x.right && `margin-left: auto;`}
  ${x => x.center && `margin-right: auto;margin-left: auto;`}
`

export default props =>
  <Label {...props}>
    {props.children}
  </Label>
