import React, {Component} from "react"
import styled from 'styled-components/native'
import { Text } from 'react-native'

import BaseModal from '../components/BaseModal'

const Title = styled.Text`
  text-align: center;
  font-size: 24px;
  font-weight: bold;
  color: #6c757d;
  margin-bottom: 3%;
`

const MenuItem = styled.TouchableOpacity`
  padding: 3% 6% 3% 6%;
  margin: 1%;
  background-color: rgba(0, 0, 0, 0.03);
  border-radius: 5px;
`

const MenuText = styled.Text`
  text-align: center;
  font-size: 18px;
  color: #6c757d;
`

class MenuModal extends Component {

  constructor(props){
    super(props)
    this.state = {
      open: true
    }
  }

  render(){
    return(
      <BaseModal>
        <Title>{this.props.title}</Title>
        <MenuItem><MenuText>Show logs for this story</MenuText></MenuItem>
        <MenuItem><MenuText>Delete story</MenuText></MenuItem>
      </BaseModal>
    )
  }

}

export default MenuModal
