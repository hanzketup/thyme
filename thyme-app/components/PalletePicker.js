import React, { Component } from 'react'
import styled from 'styled-components/native'
import { lighten, darken } from "polished"

import colors from '../constants/story_colors'

const List = styled.View`
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
  padding: 10px 0;
  margin: 0 auto;
`

const Dot = styled.TouchableOpacity`
  height: 50px;
  width: 50px;
  border: 4px solid ${x => x.selected ? darken(0.2, x.color) : lighten(0.1, x.color) };
  border-radius: 35px;
  margin: 5px;
  background-color: ${x => x.color};
  elevation: 2;
`

export default props =>
  <List>
  {colors.map(color =>
    <Dot
      selected={props.selectedColor === color}
      color={color}
      key={color}
      onPress={() => props.onPress(color)}
    />
  )}
  </List>
