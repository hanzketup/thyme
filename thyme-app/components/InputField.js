import React, { Component } from 'react'
import styled from 'styled-components/native'

const InputField = styled.TextInput`
  width: 100%;
  font-size: 21px;
  padding: 13px 20px;
  margin: 2px 0;
  border: 1px solid #ecf0f1;
  border-radius: 8px;
`

export default props =>
  <InputField {...props} />
