import React from "react"
import styled from 'styled-components/native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

const Button = styled.TouchableOpacity`
  height: 55px;
  padding: 0 25px;
  margin: 12px 0 12px 0;
  border-radius: 6px;
  elevation: 1;
  background-color: ${x => x.color};
  ${x => x.left && `margin-right: auto;`}
  ${x => x.right && `margin-left: auto;`}
  ${x => x.center && `margin-right: auto;margin-left: auto;`}
`

const InnerWrap = styled.View`
  height: 100%;
  align-self: flex-end;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`

const Title = styled.Text`
  font-size: 20px;
  font-weight: normal;
  color: #fff;
  margin-left: 25px;
  margin-top: -0.7%;
`

export default props =>
  <Button onPress={props.onPress} {...props}>
      <InnerWrap>
        <FontAwesomeIcon icon={["fal", props.icon]} color="white" size={28} />
        <Title>{props.title}</Title>
      </InnerWrap>
  </Button>
