import React from "react"
import styled from 'styled-components/native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

const Box = styled.View`
  flex: 1;
  min-height: 10%;
  width: 90%;
  padding: 3%;
  margin: 1% 5%;
  border-radius: 8px;
  background-color: ${x => x.color || "#eee"};
  flex-direction: row;
  justify-content: center;
  elevation: 4;
  z-index: 2;
`

const Message = styled.Text`
  font-size: 15px;
  color: #fff;
  padding-left: 10%;
`

const Close = styled.TouchableOpacity`
  padding: 2px 8% 2px 2px;
`

export default props =>
  <Box color={props.color}>
    <Message>{props.message}</Message>
    <Close onPress={() => props.closeAction(props.id)}><FontAwesomeIcon icon={["fal", "times"]} color="#eee" size={26} /></Close>
  </Box>
