import React from 'react'
import styled from 'styled-components/native'
import { TouchableOpacity, View, Text } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { ActivityIndicator } from 'react-native'

import { seconds_to_hours_minutes, seconds_to_hour_minute_seconds } from '../helpers/format_time'

const Wrapper = styled.TouchableOpacity`
  position: relative;
  width: 100%;
  height: 180px;
  background-color: ${x => x.color || '#596275'};
  border-radius: 8px;
  margin: 0 auto;
  elevation: 5;
`

const Inner = styled.View`
  position: relative;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
`

const Name = styled.Text`
  position: absolute;
  bottom: 8%;
  left: 5%;
  width: 60%;

  font-size: 30px;
  color: #fff;
  opacity: 0.8;
`

const Hours = styled.Text`
  position: absolute;
  top: 4%;
  right: 5%;

  font-size: 40px;
  font-weight: bold;
  color: #000;
  opacity: 0.12;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
`

const Timer = styled.Text`
  position: absolute;
  bottom: 8%;
  right: 5%;

  font-size: 18px;
  font-weight: 300;
  color: #fff;
  opacity: 0.9;
  text-align: right;
`

const PlayControl = styled.View`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 10%;
  left: 0;
  justify-content: center;
  align-items: center;
  opacity: 0.7;
`

const ContextMenu = styled.TouchableOpacity`
  position: absolute;
  top: 8%;
  left: 2%;
  padding: 2%;
`

const NotTracking = styled.Text`
  font-size: 23px;
  color: rgba(255, 255, 255, 0.8);
  text-align: center;
`

export default props =>
  <Wrapper color={props.color} activeOpacity={0.8}>
    { props.notTracking &&  <Inner><NotTracking>Select something or add {'\n'} a new story with the + below</NotTracking></Inner> }
    { props.isLoading && <Inner><ActivityIndicator style={{transform: [{ scale: 2.5 }]}} size="large" color="#fff" /></Inner> }
    { (!props.isLoading && !props.notTracking) &&
      <Inner>
        <Name>{props.name}</Name>
        <Hours>{seconds_to_hours_minutes(props.seconds)}H</Hours>
        <Timer>{seconds_to_hour_minute_seconds(props.sessionTime)}{'\n'}current session</Timer>

        <ContextMenu onPress={props.contextOnPress}>
          <FontAwesomeIcon icon={["fal", "ellipsis-v"]} size={40} color="#fff" />
        </ContextMenu>
      </Inner>
    }

  </Wrapper>
