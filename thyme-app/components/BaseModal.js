import React, {Component} from "react"
import styled from 'styled-components/native'
import { ScrollView, Modal, Text } from 'react-native'

const ModalBackground = styled.TouchableOpacity`
  height: 100%;
  width: 100%;
  justify-content: center;
  align-items: center;
`

const ModalBox = styled.View`
  min-height: 20%;
  width: 90%;
  padding: 4%;
  background: #fff;
  border-radius: 6px;
  elevation: 10;
`

class BaseModal extends Component {

  constructor(props){
    super(props)
    this.state = {
      open: true
    }
  }

  render(){
    return(
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.open}
        presentationStyle="overFullScreen"
        onRequestClose={() => this.setState({open: false})}>
          <ModalBackground pointerEvents='box-none' onPress={() => this.setState({open: false})}>
            <ModalBox>
              {this.props.children}
            </ModalBox>
          </ModalBackground>
      </Modal>
    )
  }

}

export default BaseModal
