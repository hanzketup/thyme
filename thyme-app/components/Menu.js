import React from 'react'
import styled from 'styled-components/native'
import { Image } from "react-native"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import Constants from 'expo-constants'

const Wrapper = styled.View`
  position: relative;
  height: 80px;
  margin-top: ${Constants.statusBarHeight}px;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`

const Settings = styled.TouchableOpacity`
  position: absolute;
  top: 24%;
  left: 3%;
  flex-direction: row;
  align-items: center;
  padding: 10px 10px 10px 10px;
  opacity: 0.6;
  z-index: 2;
`

const Logo = styled.Image`
  height: 45px;
`

const TimeSpan = styled.TouchableOpacity`
  position: absolute;
  top: 24%;
  right: 3%;
  flex-direction: row;
  align-items: center;
  padding: 10px 10px 10px 10px;
  opacity: 0.6;
  z-index: 2;
`

export default props =>
  <Wrapper>
    <Settings><FontAwesomeIcon icon={["fal", "cog"]} size={28} /></Settings>
    <Logo source={require('../assets/leaves.png')} resizeMode="contain" />
    <TimeSpan><FontAwesomeIcon icon={["fal", "history"]} size={28} /></TimeSpan>
  </Wrapper>
