import React from 'react'
import styled from 'styled-components/native'
import { Text, View, StyleSheet, ScrollView, KeyboardAvoidingView, SafeAreaView, Dimensions } from 'react-native'

const Wrapper = styled.KeyboardAvoidingView`
  flex: 1;
`

const Outer = styled.View`
  flex: 1;
`

const Scroll = styled.ScrollView`
  flex: 1;
`

export default props =>
  <Wrapper behavior={'padding'} keyboardVerticalOffset={0}>
    <Outer>
      <Scroll keyboardShouldPersistTaps="always" contentContainerStyle={{flexGrow: 1}}>
          {props.children}
      </Scroll>
    </Outer>
  </Wrapper>
