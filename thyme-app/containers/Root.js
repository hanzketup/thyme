import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components/native'
import Constants from 'expo-constants'

import * as userActions from '../actions/user'
import * as uiActions from '../actions/ui'
import Toast from '../components/Toast'

const Wrap = styled.View`
  flex: 1;
  z-index: 1;
`

const ToastWrap = styled.View`
  position: absolute;
  top: ${Constants.statusBarHeight + 10}px;
  right: 0;
  left: 0;
  flex-direction: column;
`

class Root extends Component {

  componentDidMount(){
    this.props.check_authentication()
  }

  render () {
    return (
      <Wrap>
        <ToastWrap>
          {this.props.toasts.map(toast => <Toast key={toast.id} closeAction={this.props.remove_toast} {...toast} />)}
        </ToastWrap>
        {this.props.children}
      </Wrap>
    )
  }

}

const mapStateToProps = state => ({
  toasts: state.ui.toasts
})

const mapDispatchToProps = {
  check_authentication:  userActions.check_authentication,
  remove_toast: uiActions.remove_toast
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Root)
