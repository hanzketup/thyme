import React, { Component } from 'react'
import { Text } from 'react-native'
import { connect } from 'react-redux'
import { Notifications } from 'expo'
import styled from 'styled-components/native'
import { AsyncStorage } from 'react-native'
import { Query, Subscription } from 'react-apollo'
import gql from 'graphql-tag'

import LargeStory from '../components/LargeStory'

const ACTIVE_STORY = gql`
  query {
    activeStory {
      id
      name
      color
      total
      activeSession {
        id
        storyId
      }
    }
  }
`

const TICK = gql`
  subscription storyTicked($id: Int!) {
    storyTicked(sessionID: $id) {
      seconds
    }
  }
`

class ActiveStory extends Component {

  constructor(props){
    super(props)
    this.notification_id = null
  }

  async componentDidMount(){
    Notifications.createCategoryAsync("inBackgroundCategory", [
      {
        actionId: "stop",
        buttonTitle: "Stop"
      },
      {
        actionId: "hide",
        buttonTitle: "Hide"
      }
    ])
  }

  async componentWillUnmount(){
    if(this.notification_id){
      await Notifications.dismissNotificationAsync(this.notification_id)
    }
  }

  async updateNotification (data) {
    if (this.notification_id){
      await Notifications.dismissNotificationAsync(this.notification_id)
    }
    this.notification_id = await Notifications.presentLocalNotificationAsync({
      id: 3,
      title: `Thyme tracking is active`,
      body: `Time is being logged for story ${data.activeStory.name}`,
      categoryId: "inBackgroundCategory",
      android: {
        sticky: false,
        color: data.activeStory.color
      }
    })
  }

  render () {
    return (
      <Query query={ACTIVE_STORY} pollInterval={1000} onCompleted={this.updateNotification.bind(this)}>
        {({ loading, error, data, refetch }) => {
          if (loading) return null
          if (error) return <Text>Error! {String(error)}</Text>

          return data.activeStory === null ? <LargeStory notTracking={true}></LargeStory> : (
            <Subscription
              subscription={TICK}
              shouldResubscribe={true}
              skip={!data.activeStory.activeSession.id}
              variables={{ id: parseInt(data.activeStory.activeSession.id) }}>
              {({ data: subData, loading: subLoading }) => {
                if (!subLoading && subData.storyTicked.storyId !== data.activeStory.id){refetch()}
                return (
                <LargeStory
                  name={data.activeStory.name}
                  color={data.activeStory.color}
                  isLoading={subLoading || loading}
                  seconds={!subLoading && data.activeStory.total}
                  sessionTime={!subLoading && subData.storyTicked.seconds}
                />
            )}}
            </Subscription>
          )
        }}
      </Query>
    )
  }

}

const mapStateToProps = state => ({

})

const mapDispatchToProps = {

}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActiveStory)
