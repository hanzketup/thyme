import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import * as userActions from '../actions/user'
import InputField from '../components/InputField'
import LargeButton from '../components/LargeButton'

const Wrap = styled.View`
  width: 100%;
  align-items: center;
  padding: 0 5%;
`

const SubmitButton = styled.Button`
  padding: 6px 10px;
`

class LoginForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }
  }

  render () {
    return (
      <Wrap>
        <InputField
          onChangeText={text => this.setState({username: text})}
          placeholder='Username'
          autoCompleteType='username'
          maxLength={40}
          editable />
        <InputField
          onChangeText={text => this.setState({password: text})}
          placeholder='Password'
          autoCompleteType='password'
          maxLength={40}
          secureTextEntry
          editable
        />
        <LargeButton right
          title="Login"
          icon="sign-in"
          color="#00b894"
          onPress={() => this.props.retrive_token(this.state.username, this.state.password)}
        />
      </Wrap>
    )
  }

}

const mapStateToProps = state => ({

})

const mapDispatchToProps = {
  retrive_token: userActions.retrive_token
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm)
