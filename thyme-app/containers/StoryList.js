import React, { Component } from 'react'
import { Text } from 'react-native'
import { connect } from 'react-redux'
import styled from 'styled-components/native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { Query, Mutation } from 'react-apollo'
import gql from 'graphql-tag'

import NavigationService from '../NavigationService'
import * as storyActions from '../actions/story'
import SmallStory from '../components/SmallStory'

const StoryListView = styled.View`
  width: 100%;
  justify-content: space-between;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 2%;
`

const NewButton = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  paddingTop: 5%;
`

const GET_STORIES = gql`
  query {
    stories {
      id
      createdAt
      name
      color
      total
    }
    activeStory {
      id
    }
  }
`

const SET_ACTIVE = gql`
  mutation setActiveStory($id: Int!) {
    setActiveStory(storyId: $id){name}
  }
`;

class StoryList extends Component {

  render () {
    return (
    <Mutation mutation={SET_ACTIVE}>
    {(setActiveStory) => (
      <Query query={GET_STORIES} fetchPolicy="no-cache" errorPolicy="all">
        {({ loading, error, data, refetch }) => {
          if (loading) return null
          if (error) return <Text>Error! {String(error)}</Text>

          data.stories.map(x => console.log(x.createdAt))

          return (
            <StoryListView>
              { data.stories
                .sort((a, b) => new Date(b.date) - new Date(a.date))
                .map(story =>
                <SmallStory
                  key={story.id}
                  active={data.activeStory.id === story.id}
                  onPress={async () => {
                    await setActiveStory({ variables: { id: parseInt(story.id) } })
                    refetch()
                  }}
                  name={story.name}
                  color={story.color}
                  seconds={story.total}
                />
              )}
              <NewButton><FontAwesomeIcon onPress={() => NavigationService.navigate("NewStory")} icon={["fal", "plus-circle"]} color="#eee" size={38} /></NewButton>
            </StoryListView>
          )
        }}
      </Query>
    )}
    </Mutation>
    )
  }

}

const mapStateToProps = state => ({

})

const mapDispatchToProps = {

}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoryList)
