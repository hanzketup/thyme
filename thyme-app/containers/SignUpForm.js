import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AsyncStorage } from 'react-native'
import styled from 'styled-components/native'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

import NavigationService from '../NavigationService'
import * as storyActions from '../actions/story'
import InputField from '../components/InputField'
import Label from '../components/Label'
import PalletePicker from '../components/PalletePicker'
import LargeButton from '../components/LargeButton'

const Wrap = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 5%;
  margin-top: 20%;
`

const Logo = styled.Image`
  height: 55px;
`

const Title = styled.Text`
  font-size: 45px;
  font-weight: 700;
  color: #00b894;
  text-align: center;
  marginBottom: 20px;
`

const Legal = styled.Text`
  font-size: 16px;
  font-weight: 700;
  text-decoration: underline;
  color: #596275;
  text-align: center;
  margin: 20px 0 5px 0;
`

const CREATE_STORY = gql`
  mutation signUp($username: String!, $password: String!, $email: String!) {
    signUp(username: $username, password: $password, email: $email){
      token
    }
  }
`

class SignUpForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      username: '',
      email: '',
      password: ''
    }
  }

  render () {
    return (
      <Mutation mutation={CREATE_STORY}>
        {(signUp) => (
          <Wrap>

            <Logo source={require('../assets/leaves.png')} resizeMode='contain' />
            <Title>Glad to have you here!</Title>

            <Label left>Give yourself a username:</Label>
            <InputField
              onChangeText={text => this.setState({username: text})}
              placeholder='Name'
              autoCompleteType='name'
              autoCapitalize="none"
              maxLength={20}
              editable />

            <Label left>And your email, so we can keep in touch:</Label>
            <InputField
              onChangeText={text => this.setState({email: text})}
              placeholder='Email'
              autoCompleteType='email'
              keyboardType="email-address"
              autoCapitalize="none"
              maxLength={40}
              editable />

            <Label left>And finally a password:</Label>
            <InputField
              onChangeText={text => this.setState({password: text})}
              placeholder='Password'
              autoCompleteType='password'
              secureTextEntry={true}
              maxLength={20}
              editable />

            <Legal onPress={() => NavigationService.navigate('PrivacyPolicy')}>By clicking the button above you agree that you have read and accept our Privacy Policy</Legal>

            <LargeButton center
              title='Sign me up!'
              icon='user-astronaut'
              color='#786fa6'
              onPress={async () => {
                let sign_up_data = await signUp({ variables: this.state })
                console.log(sign_up_data)
                await AsyncStorage.setItem('token', sign_up_data.data.signUp.token)
                NavigationService.navigate('Main')
              }}
            />

          </Wrap>
        )}
      </Mutation>
    )
  }

}

const mapStateToProps = state => ({

})

const mapDispatchToProps = {
  create_story: storyActions.create_story
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpForm)
