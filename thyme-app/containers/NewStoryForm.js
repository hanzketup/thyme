import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components/native'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

import NavigationService from '../NavigationService'
import * as storyActions from '../actions/story'
import InputField from '../components/InputField'
import Label from '../components/Label'
import PalletePicker from '../components/PalletePicker'
import LargeButton from '../components/LargeButton'

const Wrap = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 5%;
  margin-top: 20%;
`

const Logo = styled.Image`
  height: 55px;
`

const Title = styled.Text`
  font-size: 45px;
  font-weight: 700;
  color: #00b894;
  text-align: center;
  marginBottom: 20px;
`

const CREATE_STORY = gql`
  mutation createStory($name: String!, $color: String!){
    createStory(name: $name color: $color){
      name
    }
  }
`

class NewStoryForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      name: '',
      color: '#778beb'
    }
  }

  render () {
    return (
      <Mutation mutation={CREATE_STORY}>
        {(createStory) => (
          <Wrap>

            <Logo source={require('../assets/leaves.png')} resizeMode='contain' />
            <Title>Create a new Story!</Title>

            <Label left>Name your story, keep it short:</Label>
            <InputField
              onChangeText={text => this.setState({name: text})}
              placeholder='Name'
              autoCompleteType='name'
              maxLength={40}
              editable />

            <Label left>Give your story a color:</Label>
            <PalletePicker
              selectedColor={this.state.color}
              onPress={color => this.setState({color: color})}
            />

            <LargeButton center
              title="Make it happen!"
              icon="layer-plus"
              color={this.state.color}
              onPress={async () => {
                await createStory({ variables: { name: this.state.name, color: this.state.color } })
                NavigationService.navigate("Main")
              }}
            />
          </Wrap>
        )}
      </Mutation>
    )
  }

}

const mapStateToProps = state => ({

})

const mapDispatchToProps = {
  create_story: storyActions.create_story
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewStoryForm)
