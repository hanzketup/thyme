import moment from 'moment'

export const seconds_to_hours_minutes = sec => {
  var date = new Date(null);
  date.setSeconds(sec)
  return date.toISOString().substr(11, 5)
}

export const seconds_to_hour_minute_seconds = sec => {
  var date = new Date(null);
  date.setSeconds(sec)
  return date.toISOString().substr(11, 8)
}
